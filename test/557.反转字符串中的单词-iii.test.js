const reverseWords = require('../557.反转字符串中的单词-iii')
test('557.反转字符串中的单词-iii', () => {
  expect(reverseWords("Let's take LeetCode contest")).toBe(
    "s'teL ekat edoCteeL tsetnoc")
})
